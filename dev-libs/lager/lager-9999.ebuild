
EAPI=7

inherit cmake git-r3

LICENSE="MIT"
DESCRIPTION="C++ library for value-oriented design using the unidirectional data-flow architecture -- Redux for C++"
HOMEPAGE="https://github.com/arximboldi/lager"
EGIT_REPO_URI="https://github.com/arximboldi/lager.git"
EGIT_SUBMODULES=()

SLOT="0/9999"
KEYWORDS=""
IUSE="test"

DEPEND="
    dev-libs/zug:*
    dev-libs/boost:*
"
RDEPEND="${DEPEND}"
BDEPEND=""

src_configure() {
    local mycmakeargs=(
        -Dlager_BUILD_TESTS=$(usex test 'ON' 'OFF')
        -Dlager_BUILD_EXAMPLES=OFF
        -Dlager_BUILD_DOCS=OFF
    )

    cmake_src_configure
}
