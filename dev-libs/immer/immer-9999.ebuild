
EAPI=7

inherit cmake git-r3

LICENSE="Boost-1.0"
DESCRIPTION="Postmodern immutable and persistent data structures for C++ -- value semantics at scale"
HOMEPAGE="https://github.com/arximboldi/immer"
EGIT_REPO_URI="https://github.com/arximboldi/immer.git"
EGIT_SUBMODULES=()

SLOT="0/9999"
KEYWORDS=""
IUSE="test"

DEPEND=""
RDEPEND="${DEPEND}"
BDEPEND=""

src_configure() {
    local mycmakeargs=(
        -Dimmer_BUILD_TESTS=$(usex test 'ON' 'OFF')
        -Dimmer_BUILD_EXAMPLES=OFF
        -Dimmer_BUILD_DOCS=OFF
        -Dimmer_BUILD_EXTRAS=OFF
    )

    cmake_src_configure
}
