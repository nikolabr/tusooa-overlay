
EAPI=7

inherit cmake git-r3

LICENSE="AGPL-3+"
DESCRIPTION="A matrix client sdk built upon lager"
HOMEPAGE="https://gitlab.com/kazv/libkazv"
EGIT_REPO_URI="https://gitlab.com/kazv/libkazv.git"
EGIT_SUBMODULES=()
EGIT_COMMIT="v${PV}"

SLOT="0/0.1"
KEYWORDS="~amd64"
IUSE="test kazvjob"

REQUIRED_USE="test? ( kazvjob )"

DEPEND="
    dev-libs/zug:=
    dev-libs/lager:=
    dev-libs/immer:=
    dev-libs/boost:=
    dev-cpp/nlohmann_json:=
    dev-libs/cereal:=
    dev-libs/olm:=
    test? ( =dev-cpp/catch-2* )
    kazvjob? ( =net-libs/cpr-1.5*:= )
"
RDEPEND="${DEPEND}"
BDEPEND=""

src_configure() {
    local mycmakeargs=(
        -Dlibkazv_BUILD_TESTS=$(usex test 'ON' 'OFF')
        -Dlibkazv_BUILD_EXAMPLES=OFF
        -Dlibkazv_BUILD_KAZVJOB=$(usex kazvjob 'ON' 'OFF')
    )

    cmake_src_configure
}

src_test() {
    ./src/tests/kazvtest || die
}
