
EAPI=7

inherit cmake

LICENSE="MIT"
DESCRIPTION="C++ Requests: Curl for People, a spiritual port of Python Requests"
HOMEPAGE="http://whoshuu.github.io/cpr/"
SRC_URI="https://github.com/whoshuu/cpr/archive/1.5.2.tar.gz"

SLOT="0/1.5.2"
KEYWORDS="~amd64"
IUSE="test"

DEPEND="
    net-misc/curl
"
RDEPEND="${DEPEND}"
BDEPEND=""

src_configure() {
    local mycmakeargs=(
        -DUSE_SYSTEM_CURL=ON
        -DBUILD_CPR_TESTS=$(usex test ON OFF)
        -DBUILD_SHARED_LIBS=ON
    )

    cmake_src_configure
}
