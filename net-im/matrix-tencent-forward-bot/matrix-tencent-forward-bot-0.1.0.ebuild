
EAPI=7

inherit cmake git-r3

LICENSE="AGPL-3+"
DESCRIPTION="A forward bot between Matrix and Tencent QQ"
HOMEPAGE="https://lily.kazv.moe/kazv/matrix-tencent-forward-bot"
EGIT_REPO_URI="https://lily.kazv.moe/kazv/matrix-tencent-forward-bot.git"
EGIT_SUBMODULES=()
EGIT_COMMIT="v${PV}"

SLOT="0"
KEYWORDS="~amd64"

DEPEND="
    dev-libs/zug:=
    dev-libs/immer:=
    dev-cpp/nlohmann_json:=
    net-libs/libkazv:=[kazvjob]
"
RDEPEND="${DEPEND}"
BDEPEND=""

src_configure() {
    local mycmakeargs=(
    )

    cmake_src_configure
}

pkg_postinst() {
    elog "To use mtfb you will need to set up OneBot Kotlin manually."
    elog "Please see https://github.com/yyuueexxiinngg/onebot-kotlin"
    elog "for reference."
}
