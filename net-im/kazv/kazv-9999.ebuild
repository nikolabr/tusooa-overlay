
EAPI=7

inherit cmake git-r3

LICENSE="AGPL-3+"
DESCRIPTION="A matrix client using libkazv"
HOMEPAGE="https://lily.kazv.moe/kazv/kazv"
EGIT_REPO_URI="https://lily.kazv.moe/kazv/kazv.git"
EGIT_SUBMODULES=()

SLOT="0/9999"
KEYWORDS=""

DEPEND="
    dev-libs/zug:=
    dev-libs/immer:=
    dev-cpp/nlohmann_json:=
    net-libs/libkazv:=[kazvjob]
    dev-qt/qtgui:=
    dev-qt/qtcore:=
    dev-qt/qtquickcontrols2:=
    dev-qt/qtdeclarative:=
    kde-frameworks/kirigami:=
    kde-frameworks/extra-cmake-modules
"
RDEPEND="${DEPEND}"
BDEPEND=""

src_configure() {
    local mycmakeargs=(
    )

    cmake_src_configure
}
